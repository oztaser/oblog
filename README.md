# oblog
Geliştirdiğim ilk django uygulaması

![blog](http://i.imgur.com/lCwEBiF.png?1)

#Kurulum
Virtual Environment oluşturmak

    virtualenv blog
    source blog/bin/active

Depoyu klonlamak ve gereksinimlerini kurmak

    git clone git@github.com:oztaser/oblog.git
    pip install -r oblog/requirements.pip
    
Uygulamayı başlatmak
    
    python manage.py migrate
    python manage.pr runserver

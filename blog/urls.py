from django.conf.urls import patterns, include, url
from django.contrib import admin

from . import views



urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'oblog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    (r'^$', views.index),
    url(r'^(?P<post_slug>[a-zA-Z0-9_.-]+)/$', views.post_detail),
    url(r'^tag/(?P<tag_slug>[a-zA-Z0-9_.-]+)/$', views.tag_page),
    url(r'^page/(?P<page_slug>[a-zA-Z0-9_.-]+)/$', views.page_detail),
    url(r'^category/(?P<category_slug>[a-zA-Z0-0_.-]+)/$', views.category_page),

)

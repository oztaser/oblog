from django.db import models
from ckeditor.fields import RichTextField

class PostKeyword(models.Model):
    keyword     = models.CharField(max_length=250)
    slug        = models.SlugField(unique=True)

    def __unicode__(self):              # __unicode__ on Python 2
        return self.keyword

class Post(models.Model):
    title       = models.CharField(max_length=200,unique=True)
    slug        = models.SlugField()
    content     = RichTextField()
    category    = models.ForeignKey('blog.PostCategory')
    description = models.CharField(max_length=350,blank=True)
    keywords    = models.ManyToManyField(PostKeyword,blank=True)
    pub_date    = models.DateField(auto_now_add=True)
    mod_date    = models.DateField(auto_now=True)

    def __unicode__(self):              # __unicode__ on Python 2
        return self.title


class PostCategory(models.Model):
    title       = models.CharField(max_length=200,unique=True)
    slug        = models.SlugField()
    description = models.CharField(max_length=350,blank=True)
    keywords    = models.CharField(max_length=300,blank=True)
    pub_date    = models.DateField(auto_now_add=True)

    def __unicode__(self):              # __unicode__ on Python 2
        return self.title

class Page(models.Model):
    title       = models.CharField(max_length=200,unique=True)
    slug        = models.SlugField()
    content     = RichTextField()
    description = models.CharField(max_length=350,blank=True)
    display_menu_choices = (
        ('y', 'Yes'),
        ('n', 'No'),
    )
    display_menu = models.CharField(max_length=2,choices=display_menu_choices, default='y')

    def __unicode__(self):              # __unicode__ on Python 2
        return self.title

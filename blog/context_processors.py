from blog.models import Page


def page_list(request):
    all_page = Page.objects.filter(display_menu = 'y')

    return {
        'page_list': all_page,
    }

from django.shortcuts import render, render_to_response, get_object_or_404
from .models import Post, PostKeyword, PostCategory, Page
from django.template import Template, RequestContext


def index(request):
    try:
        post = Post.objects.order_by('-pub_date')
    except Post.DoesNotExist:
        post = None

    return render(request, 'index.html', {'post_list' : post})

def post_detail(request, post_slug):

##  post = get_object_or_404(Post, slug=slug)
##  tags = PostKeyword.objects.all().prefetch_related('toppings')

    post = Post.objects.get(slug = post_slug)
    post_keywords = post.keywords.all()
    post_category = PostCategory.objects.get(title = post.category)

    return render(request, 'post_detail.html', {'post': post, 'keywords': post_keywords, 'category': post_category})


def tag_page(request, tag_slug):
    tag = PostKeyword.objects.get(slug = tag_slug)
    tagged_post = Post.objects.filter(keywords = tag).order_by('-pub_date')

    return render(request, 'tag_page.html', {'post_list': tagged_post, 'keyword': tag})

def category_page(request, category_slug):
    category = PostCategory.objects.get(slug = category_slug)
    category_post = Post.objects.filter(category = category)

    return render(request, 'category_page.html', {'post_list':category_post, 'category':category})


def page_detail(request, page_slug):
    page = Page.objects.get(slug = page_slug)
    return render(request, 'page_detail.html', {'page': page})

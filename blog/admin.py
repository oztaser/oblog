from django.contrib import admin
from blog.models import PostKeyword, Post, PostCategory, Page

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug','pub_date','mod_date')
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None,{
            'fields': ('title','slug','content','category','description','keywords')
        }),
    )

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title','slug','pub_date')
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None,{
            'fields': ('title','slug','description','keywords')
        }),
    )

class PostKeywordAdmin(admin.ModelAdmin):
    list_display = ('keyword',)
    prepopulated_fields = {'slug': ('keyword',)}
    fieldsets = (
        (None,{
                'fields': ('keyword','slug')
        }),
    )

class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug','display_menu')
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None,{
                'fields': ('title','slug','content','description','display_menu')
        }),
    )

admin.site.register(Page,PageAdmin)
admin.site.register(PostCategory,CategoryAdmin)
admin.site.register(Post,PostAdmin)
admin.site.register(PostKeyword,PostKeywordAdmin)
